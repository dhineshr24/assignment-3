from dataclasses import dataclass

@dataclass
class Basket(object):
    items:list

    def total(self):
        return 0